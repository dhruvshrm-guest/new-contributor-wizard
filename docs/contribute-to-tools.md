# New Contributor Wizard - Contribute To Tools

This tutorial might be overwhelming, in case of any assistance reach out to the Maintainers of this project.

## Step 1 - Installing The Application

Make sure you follow [Developer](developer.md) docs to build the application from the source on your local machine. Once you are setup and application works properly move to the next step.

## Step 2 - Prerequisites

Every tool is very carefully crafted with [Kivy](https://kivy.org/docs/gettingstarted/intro.html) and other Python logic to work perfectly. In order to contribute Tools for any module in this application you should know atleast basics of Python and Kivy framework. This application is using Python v3.5 and Kivy v1.10.1 to work. Other dependencies can be read from [Pipfile](../Pipfile) and [Pipfile.lock](../Pipfile.lock) from the repository itself.

## Step 3 - Understanding The Codebase

The codebase is maintained keeping modular structure in mind. Here are few of the important modules you should know about in order to contribute Tools.

- `ui` directory contains only the GUI which is essential Kivy Lang written inside `.kv` files

- `modules` directory contains all the Python/Kivy logic required to integrate GUI with the entire application

- `modules/course_modules` directory contains modules for each courseware. This is where we keep all our Tutorials and Tools

- `parser` directory contains Tutorial Parser. This is what is used to parse Tutorial JSON files and build Kivy GUI. This makes easy for folks to build Tutorials for the application. To find out more about this read [Contribute To Tutorials](contribute-to-tutorials.md)

- `module_box` directory contains modules which help build Menu and Navigation for each courseware modules. So everytime you select a courseware, these modules are used to switch between Tutorials and Tools of the respective courseware and provide list of Tutorials and Tools

- `signin`, `signup` and `dashboard` are self explanatory directory names

You might have guessed that your focus should be on `course_modules` and `ui` only.

## Step 4 - Updating Tools Index

Before making your Tool, first step is to list your tool. This is done in `data/module_data.json`. Let's say you want to add a new tool to `Encryption`, you have to find `encryption` key in `data/module_data.json` which would look something like below

```json
{"encryption":
    {
        "tutorials": [
            {
                "title": "Encryption 101",
                "difficulty": "Beginner"
            }
        ],
        "tools": [
            {
                "title": "Display and manage key pair",
                "difficulty": "Beginner"
            },
            {
                "title": "Create key pair",
                "difficulty": "Beginner"
            },
            {
                "title": "Encrypt a message",
                "difficulty": "Intermediate"
            },
            {
                "title": "Decrypt a message",
                "difficulty": "Intermediate"
            }
        ]
    }
}
```

You can see there are already 4 tools listed as the value to `tools`, you simply have to add another `{}` object with two key value pair. First key should be 'title' whose value should contain the name you have decided for your tool. Second key should be `difficulty` whose value should either be `Begineer`, `Intermdiate` or `Advance` which is self explanatory. That's it.

So, let's say you want to add a Tool named `My Awesome Tool` with a difficulty `Intermediate`, below is how you'll list it in JSON after other tools present in `encryption`.

```json
...
    "tutorials": [
            {
                "title": "Decrypt a message",
                "difficulty": "Intermediate"
            },
            {
                "title": "My Awesome Tool",
                "difficulty": "Intermediate"
            }
            ...
```

## Step 5 - Adding Tool Module

After listing your Tool, now's the time to create module (the `.py` file) for your Tool. The location in which your Tool module should be saved can be derived from the table below

| Module in which you want to add your Tool | Folder to add your Tutorial module (`.py`) file |
| ----------------------------------------- | ----------------------------------------------- |
| Blog                                      | `modules/course_modules/blog/tools/`            |
| CLI                                       | `modules/course_modules/cli/tools/`             |
| Communication                             | `modules/course_modules/communication/tools/`   |
| Encryption                                | `modules/course_modules/encryption/tools/`      |
| How To Use                                | `modules/course_modules/how_to_use/tools/`      |
| VCS                                       | `modules/course_modules/vcs/tools/`             |
| Way Ahead                                 | `modules/course_modules/way_ahead/tools/`       |

## Step 6 - Naming The Module

After you have found the place to put your module (`.py`) in, you have to figure out the name of the file as well. Let's look at what are the necessary 

1. Name of the Tool module file should be in lowercase of the title given to the Tutorial in `data/module_data.json` in Step 4

2. Spaces in the Name should be replaced by underscore `_`

3. Add `.py` at the end of the filename. So, going by the above example the filename would be `my_awesome_tool.py`

4. This module file should contains atleast one compulsory class. This class name should be the name of title in Capital-Case with the spaces removed, for example `MyAwesomeTool` or simply add the following code to begin with


```python
class MyAwesomeTool(object):
    pass
```


Now, when you'll run the application and visit `Encryption` and press `Tools`, this is what you'll see you Tool listed. If you try to `Open` it the application will crash, it's suppose to happen since out module file empty.

![Encryption Tools Menu](https://i.imgur.com/rLzcH9i.png)

### Step 7 - Integrating The UI

Now since the module is ready to be dealt with let's create some UI. There are 2 ways in which you can work on the UI, either you declare the UI inside the module file or create a separate `.kv` file containing Kivy lang and import it inside the module. The way this application is build the UI has been kept apart from the modules inside `ui` directory. And if you probe a little you'll find `ui/encryption` directory containing UI files for pre-existing Encryption Tools. So, it's better to add a `.kv` file for your module there. You can follow the above nomenclature to name your `.kv` file as well for better readability. Once you are done you can restart the application and `Open` your tool from the menu to see the respective UI you wrote in action.

### Step 8 - Finishing Up

Moving ahead, you can interact with the UI from the module file you created and perform respective actions for which your Tool is build. Make sure to raise a merge request after you are satisfied with your changes so that the world can take advantage of your Awesome Tool.
