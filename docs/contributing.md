# New Contributor Wizard - Contributor Documentation

### Few Key Points

- In order to build the application from the source provided in this repository or to know more about important files and locations, must read [Developer's Doc](developer.md).

- Again, read [Developer's Doc](developer.md) as it's very important that you understand the reason why everything at it's place.

- In order to contribute to designing of the application read [Design Doc](design.md)

- Always create a Fork of this repository and then start to make changes to the forked repository.

- Once you are satisfied with the changes, create a merge request describing briefly about all the changes you have made.

- Make sure the merge reqeust is atomic, ie. only trying to solve one problem at a time.

- Always write tests, documentations and changelogs (if required) with the changes you make to the project.

### Contribute To Tutorials

Contributing Tutorials is as simple as creating a JSON file, NO CODING REQUIRED! You can create a JSON file in order to contribute to Tutorials of any module of your choice. Visit [Contribute To Tutorials](contribute-to-tutorials.md) to know how.

### Contribute To Tools

You can contribute new tools in any module of you choice. To know how visit [Contribute To Tools](contribute-to-tools.md).
